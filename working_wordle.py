import requests as rq
import random

DEBUG = False

class WordleBot:
    words = [word.strip() for word in open("words.txt")]
    wordle_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = wordle_url + "register"
    creat_url = wordle_url + "create"
    guess_url = wordle_url + "guess"

    def __init__(self, name: str):
        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(WordleBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(WordleBot.creat_url, json=creat_dict)

        self.choices = WordleBot.words[:]
        random.shuffle(self.choices)

    def play(self) -> str:
        def post(choice: str) -> tuple[str, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(WordleBot.guess_url, json=guess)
            rj = response.json()
            feedback = rj["feedback"]
            status = "GGGGG" in feedback
            return feedback, status

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        feedback, won = post(choice)
        tries = [f'{choice}:{feedback}']

        attempts = 1
        while not won and attempts < 6:
            if DEBUG:
                print(choice, feedback, self.choices[:10])
            self.update(choice, feedback)
            if not self.choices:
                print("No valid words left to guess.")
                break
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            feedback, won = post(choice)
            tries.append(f'{choice}:{feedback}')
            attempts += 1

        print("Secret is", choice, "found in", len(tries), "attempts")
        print("Route is:", " => ".join(tries))

    def update(self, choice: str, feedback: str):
        greens = {i: c for i, (c, f) in enumerate(zip(choice, feedback)) if f == 'G'}
        yellows = {i: c for i, (c, f) in enumerate(zip(choice, feedback)) if f == 'Y'}
        reds = {c for i, (c, f) in enumerate(zip(choice, feedback)) if f == 'R'}

        def word_matches(word: str) -> bool:
            for i, c in greens.items():
                if word[i] != c:
                    return False
            for i, c in yellows.items():
                if c not in word or word[i] == c:
                    return False
            if any(c in word for c in reds):
                return False
            return True

        self.choices = [w for w in self.choices if word_matches(w)]

game = WordleBot("CodeShifu")
game.play()
