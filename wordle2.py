import requests as rq
import random
DEBUG = False

class WordleBot:
    words = [word.strip() for word in open("words.txt")]
    wordle_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = wordle_url + "register"
    creat_url = wordle_url + "create"
    guess_url = wordle_url + "guess"

    def __init__(self, name: str):

        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(WordleBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(WordleBot.creat_url, json=creat_dict)

        self.choices = WordleBot.words[:]
        random.shuffle(self.choices)

    def play(self) -> str:
        def post(choice: str) -> tuple[int, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(WordleBot.guess_url, json=guess)
            rj = response.json()
            right = rj["feedback"]
            status = "win" in rj["message"]
            return right, status

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        right, won = post(choice)
        tries = [f'{choice}:{right}']

        for _ in range(6):
            if DEBUG:
                print(choice, right, self.choices[:10])
            self.update(choice, right)
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            right, won = post(choice)
            tries.append(f'{choice}:{right}')
        if won:
            print("Secret is", choice, "found in", len(tries), "attempts")
            print("Route is:", " => ".join(tries))
        else:
            print("Not found")

    def update(self, choice: str, right: int):
        def common(choice: str, word: str):
            return len(set(choice) & set(word))
        for letter, pos in enumerate(right):
            if letter == "G":
                self.choices = [w for w in self.choices if w[pos] == letter]
            elif letter == "Y":
                self.choices = [w for w in self.choices if letter in w]
            else:
                self.choices = [w for w in self.choices if letter not in w]


game = WordleBot("CodeShifu")
game.play()