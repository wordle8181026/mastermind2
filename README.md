Team : Anishka, Snigdha, Saathviga and Jyoti

This game is a clone of the word guessing challenge, Wordle. You have six attempts to guess a five-letter word. After each guess, you'll receive feedback in the form of color-coded hints:

Green(G): The letter is both in the word and in the correct position.
Yellow(Y): The letter is in the word but not in the correct position.
Red(R): The letter is not in the word at all.

Use these clues to help narrow down your guesses and solve the puzzle!
